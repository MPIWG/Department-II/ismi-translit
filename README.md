# ISMI-translit

Tools for processing the ISMI arabic transliteration format. For the ISMI mapping of Arabic to Latin characters see [this table](https://ismi.mpiwg-berlin.mpg.de/sites/default/files/2018-11/Arabic_Latin_conversion.pdf).

The normalization rules for the ISMI transliteration format are documented in [ismi-translit-normalization](ismi-translit-normalization.md).

The rules to transform the ISMI transliteration format into US Library of Congress (LOC) transliteration format are documented in [ismi-translit-loc](ismi-translit-loc.md).

## Lucene Analyzer

The `lucene` directory contains the [Lucene](https://lucene.apache.org/) Analyzer [IsmiTranslitAnalyzer](lucene/src/main/java/de/mpg/mpiwg/ismi/analyzer/IsmiTranslitAnalyzer.java) using the Java implementation of the normalization rules in [ArabicTranslitNormalizer](lucene/src/main/java/de/mpg/mpiwg/ismi/normalizer/ArabicTranslitNormalizer.java)

### Include normalization in Blazegraph full text search

You can include the IsmiTranslitAnalyzer in the [Blazegraph](https://github.com/blazegraph/database) full text search using the [configurable text analyzer](https://wiki.blazegraph.com/wiki/index.php/Text_Indexing_Configuration_Options).

First you need to include the ISMI analyzer JAR file in the Blazegraph library path.

Then add the following to the `RWStore.properties`:

```
com.bigdata.search.FullTextIndex.analyzerFactoryClass=com.bigdata.search.ConfigurableAnalyzerFactory
com.bigdata.search.ConfigurableAnalyzerFactory.analyzer._.like=eng
com.bigdata.search.ConfigurableAnalyzerFactory.analyzer.en.like=eng
com.bigdata.search.ConfigurableAnalyzerFactory.analyzer.eng.analyzerClass=org.apache.lucene.analysis.standard.StandardAnalyzer
com.bigdata.search.ConfigurableAnalyzerFactory.analyzer.ar-latn.analyzerClass=de.mpg.mpiwg.ismi.analyzer.IsmiTranslitAnalyzer
```

This uses the `IsmiTranslitAnalyzer` for literals with the language tag `ar-latn` and the Lucene `StandardAnalyzer` for all other literals.

Note: Delete the blazegraph.jnl file and re-create the store after you change the properties.

### Use with Maven

Artifact:

```
<groupId>de.mpg.mpiwg.ismi</groupId>
<artifactId>ismi-translit-analyzer</artifactId>
<version>1.0.0-SNAPSHOT</version>
```

Repository:

```
<repository>
	<id>gwdg-gitlab</id>
	<url>https://gitlab.gwdg.de/api/v4/projects/8243/packages/maven</url>
</repository>
```

## Drupal modules

The `drupal` directory contains [Drupal 8](https://drupal.org) modules:

### search_api_transliteration

ISMI Arabic translit normalization as a transliteration service plugin for the `search_api` module.

### ismi_formatter

Multiple field formatters:

* `ismi_date_field_formatter`, "ISMI date": formats a date as Hijra / Gregorian.
* `romanized_translit_formatter`, "Romanized Arabic text": formats ISMI translit text as LOC transliteration.
* `romanized_entity_reference_label_formatter`, "Romanized Arabic label": formats ISMI translit labels as LOC transliteration.
* `romanized_entity_reference_label2_formatter`, "Romanized Arabic label with label": formats ISMI translit labels of labels as LOC transliteration.


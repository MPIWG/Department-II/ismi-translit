<?php

namespace Drupal\ismi_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\entity_path_formatter\Plugin\Field\FieldFormatter\EntityReferenceWithLabelFormatter;

/**
 * Plugin implementation of the 'romanized_entity_reference_label2_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "romanized_label2_formatter",
 *   label = @Translation("Romanized Arabic label with label"),
 *   description = @Translation("Display the label of the referenced entities as romanized transliterated Arabic under a custom label."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class RomanizedEntityReferenceLabel2Formatter extends EntityReferenceWithLabelFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // let default label formatter do the work
    $elements = parent::viewElements($items, $langcode);

    // romanize the reference titles
    foreach ($elements as &$elem) {
      RomanizedTranslitFieldFormatter::romanizeViewElement($elem);
    }

    // then add the field title
    foreach ($items as $delta => $value) {
      $elements['#title'] = $this->t($this->getSetting('label'));
    }
    
    return $elements;
  }

}

<?php

namespace Drupal\ismi_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;

/**
 * Plugin implementation of the 'romanized_entity_reference_label_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "romanized_label_formatter",
 *   label = @Translation("Romanized Arabic label"),
 *   description = @Translation("Display the label of the referenced entities as romanized transliterated Arabic."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class RomanizedEntityReferenceLabelFormatter extends EntityReferenceLabelFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // let default label formatter do the work
    $elements = parent::viewElements($items, $langcode);

    // change the link text
    foreach ($elements as &$elem) {
      RomanizedTranslitFieldFormatter::romanizeViewElement($elem);
    }
    return $elements;
  }

}

<?php

namespace Drupal\ismi_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use IntlDateFormatter;
use DateTime;

/**
 * Plugin implementation of the 'ismi_date_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "ismi_date_field_formatter",
 *   label = @Translation("ISMI date"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class IsmiDateFieldFormatter extends FormatterBase {

  protected $hijriDateFormatter;
  protected $julianDateFormatter;

  /**
   *
   * {@inheritdoc}
   * @see \Drupal\Core\Field\FormatterBase::__construct()
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    // create date formatters
    $this->hijriDateFormatter = IntlDateFormatter::create('en@calendar=islamic', 
        IntlDateFormatter::SHORT, // default date format
        IntlDateFormatter::NONE, // default time format
        'GMT', // default timezone
        IntlDateFormatter::TRADITIONAL, // calendar, needs to be TRADITIONAL
        "y"); // use year only
    $this->julianDateFormatter = IntlDateFormatter::create('en', 
        IntlDateFormatter::SHORT, // default date format
        IntlDateFormatter::NONE, // default time format
        'GMT', // default timezone
        IntlDateFormatter::GREGORIAN, // calendar
        "y"); // use year only
        
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    if (method_exists($item->date, 'getPhpDateTime')) {
      // should be in Drupal 8.6 ...
      $date = $item->date->getPhpDateTime();
    } else {
      // re-parse DrupalDateTime into PHP DateTime...
      $date = new DateTime($item->value);
    }
    
    // format date
    $hijri = $this->hijriDateFormatter->format($date);
    $julian = $this->julianDateFormatter->format($date);
    // Julian/Gregorian calendar should switch automatically
    $wc = ((int)($date->format('y')) > 1582) ? 'G' : 'J';
    
    // compose text
    $text = "${hijri}H/${julian}${wc}";
    
    // format text as HTML
    return nl2br(Html::escape($text));
  }

}

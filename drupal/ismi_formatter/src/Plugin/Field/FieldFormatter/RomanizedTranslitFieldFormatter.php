<?php

namespace Drupal\ismi_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;
use Normalizer;

/**
 * Plugin implementation of the 'romanized_translit_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "romanized_translit_formatter",
 *   label = @Translation("Romanized Arabic text"),
 *   field_types = {
 *     "string",
 *     "text"
 *   }
 * )
 */
class RomanizedTranslitFieldFormatter extends StringFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    // let default string formatter do the work
    $elements = parent::viewElements($items, $langcode);
    
    // change the text
    foreach ($elements as &$elem) {
      $this->romanizeViewElement($elem);
    }
    return $elements;
  }

  /**
   * Runs ::romanize on all text in the view element.
   *
   * @param
   *          elem
   */
  public static function romanizeViewElement(&$elem) {
    if (is_array($elem)) {
      $type = $elem['#type'];
      $text = &$elem;
      if ($type == 'link') {
        // if this is a link change its title
        $text = &$elem['#title'];
      }
      if (is_string($text)) {
        // element is simple string
        $text = RomanizedTranslitFieldFormatter::romanize($text);
      } elseif (is_array($text)) {
        if ($text['#type'] == 'inline_template') {
          // element is inline_template
          $text['#context']['value'] = RomanizedTranslitFieldFormatter::romanize($text['#context']['value']);
        }
      }
    }
  }


  /**
   * Convert ISMI transliteration into LOC romanization/transcription.
   *
   * See document: translit-to-romanization-2.0.doc by Chantal Wahbi
   * based on
   * http://www.loc.gov/catdir/cpso/romanization/arabic.pdf
   *
   * @param string $text
   * @return string
   * 
   * @author cwahbi, casties
   *
   */
  public static function romanize($text) {

    // make sure we have composed unicode
    if (!Normalizer::isNormalized($text, Normalizer::NFC)) {
      $text = Normalizer::normalize($text, Normalizer::NFC);
    }
    // make sure we have standard ayn and hamza
    // (unicode escapes in PHP need double quotes!)
    $text = preg_replace("/(\u{2018}|\u{02BB})/", "\u{02BF}", $text); // ‘|ʻ -> ʿ
    $text = preg_replace("/(\u{2019}|\u{02BC})/", "\u{02BE}", $text); // ’|ʼ -> ʾ

    /*
     *  Note: beware that not all translit characters are considered word-constituents by PHP.
     *  This can lead to problems with \b. 
     */
    
    /*
     * Rule 2c
     *
     * al-Xẗ => al-Xh
     */
    $text = preg_replace('/\b(al-)(\S+)ẗ(\s|\z)/', '$1$2h$3', $text);

    /*
     * rule 2.d
     *
     * Xẗan -> Xtan
     */
    $text = preg_replace('/(\S+)ẗan\b/', '$1tan', $text);

    /*
     * rule 2a
     *
     * [Not beginnig with: al-] Xẗ al-X => Xt al-X
     */
    $text = preg_replace('/\b((?!al-)\S+)ẗ(\s+)(al-)/', '$1t$2$3', $text);

    /*
     * rule 2b
     *
     * Xẗ [Not followed by: al-X] => Xh
     */
    $text = preg_replace('/(\S+)ẗ(\s+|(?!al-)\S*)/', '$1h$2', $text);

    /*
     * rule 4.B
     *
     * [li al-X; li’l-X; li-’l-X; li-l-X] => lil-X
     */
    $text = preg_replace('/\b(li al-|liʾl-|li-ʾl-|li-l-)(\S+)/', 'lil-$2', $text);

    /*
     * rule 4.A
     *
     * li X => li-X
     */
    $text = preg_replace('/\b(li )(\S+)/', 'li-$2', $text);

    /*
     * rule 5a
     *
     * [’l-X; X-’l-X] => al-X
     */
    $text = preg_replace('/(-?ʾl-)(\S+)/', ' al-$2', $text);

    /*
     * rule 5b
     *
     * aY-YX; Y=Sun letters[t;ṯ;d;ḏ;r;z;s;š;ṣ;ḍ;ṭ;ẓ;l;n] => al-YX
     */
    $text = preg_replace('/\b(a|A)(t-(t)|ṯ-(ṯ)|d-(d)|ḏ-(ḏ)|r-(r)|z-(z)|s-(s)|š-(š)|ṣ-(ṣ)|ḍ-(ḍ)|ṭ-(ṭ)|ẓ-(ẓ)|l-(l)|n-(n))(\S+)/', 
        '$1l-$3$4$5$6$7$8$9$10$11$12$13$14$15$16$17', $text);
    // the groups 3-16 will be empty except for the real match

    /*
     * rule 3.A
     *
     * P al-X; P=[ bi; wa; ka] => P-al-X
     * 
     * rule 3a is subsumed by 3b
     * 
     * rule 3.B
     *
     * P X; P=[ bi; wa; ka] => P-X
     */
    $text = preg_replace('/\b(bi|wa|ka)(\s+)(\S+)/', '$1-$3', $text);
      
    /*
     * rules 6 and 7 are currently not used.
     */
    
    /*
     * rule 1 (comes last)
     */
    $text = preg_replace("/\u{1E6F}/", "\u{0074}\u{0068}", $text);//ṯ -> th
    $text = preg_replace("/\u{1E6E}/", "\u{0054}\u{0068}", $text);//Ṯ -> Th
    
    $text = preg_replace("/\u{1E2B}/", "\u{006B}\u{0068}", $text);//ḫ -> kh
    $text = preg_replace("/\u{1E2A}/", "\u{004B}\u{0068}", $text);//Ḫ -> Kh
    
    $text = preg_replace("/\u{1E0F}/", "\u{0064}\u{0068}", $text);//ḏ -> dh
    $text = preg_replace("/\u{1E0E}/", "\u{0044}\u{0068}", $text);//Ḏ -> Dh
    
    $text = preg_replace("/\u{0161}/", "\u{0073}\u{0068}", $text);//š -> sh
    $text = preg_replace("/\u{0160}/", "\u{0053}\u{0068}", $text);//Š -> Sh
    
    $text = preg_replace("/\u{0121}/", "\u{0067}\u{0068}", $text);//ġ -> gh
    $text = preg_replace("/\u{0120}/", "\u{0047}\u{0068}", $text);//Ġ -> Gh
    
    $text = preg_replace("/\u{1EF3}/", "\u{00E1}", $text); // ỳ -> á
    
    return $text;
  }

}

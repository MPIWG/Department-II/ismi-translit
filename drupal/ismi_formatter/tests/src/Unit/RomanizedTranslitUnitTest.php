<?php

namespace Drupal\Tests\ismi_formatter\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\ismi_formatter\Plugin\Field\FieldFormatter\RomanizedTranslitFieldFormatter;

/**
 * Test class for RomanizedTranslit formatter methods.
 *
 * @coversDefaultClass Drupal\ismi_formatter\Plugin\Field\FieldFormatter\RomanizedTranslitFieldFormatter
 * @group ismi_formatter
 */
class RomanizedTranslitUnitTest extends UnitTestCase {

  /**
   * @covers ::romanize
   */
  public function testRomanizeHamza() {
    $this->assertEquals("ʿ|ʿ -> ʿ",
        RomanizedTranslitFieldFormatter::romanize("‘|ʻ -> ʿ"));
    $this->assertEquals("ʾ|ʾ -> ʾ",
        RomanizedTranslitFieldFormatter::romanize("’|ʼ -> ʾ"));
  }
  
  /**
   * @covers ::romanize
   */
  public function testRomanizeRule1() {
    $this->assertEquals("Th/th Kh/kh Dh/dh Sh/sh Gh/gh á", 
        RomanizedTranslitFieldFormatter::romanize("Ṯ/ṯ Ḫ/ḫ Ḏ/ḏ Š/š Ġ/ġ ỳ"));
  }
  
  /**
   * @covers ::romanize
   */
  public function testRomanizeRule2() {
    $this->assertEquals("al-risalah", RomanizedTranslitFieldFormatter::romanize("al-risalaẗ"));
    $this->assertEquals("risalat al-kabir", RomanizedTranslitFieldFormatter::romanize("risalaẗ al-kabir"));
    $this->assertEquals("risalat    al-kabir", RomanizedTranslitFieldFormatter::romanize("risalaẗ    al-kabir"));
    $this->assertEquals("al-hayʾah al-basīṭah", RomanizedTranslitFieldFormatter::romanize("al-hayʾaẗ al-basīṭaẗ"));
    $this->assertEquals("risalat al-kabirah", RomanizedTranslitFieldFormatter::romanize("risalaẗ al-kabiraẗ"));
    $this->assertEquals("risalah", RomanizedTranslitFieldFormatter::romanize("risalaẗ"));
    $this->assertEquals("risalah risalah", RomanizedTranslitFieldFormatter::romanize("risalaẗ risalaẗ"));
    $this->assertEquals("risalatan", RomanizedTranslitFieldFormatter::romanize("risalaẗan"));    
  }
  
  /**
   * @covers ::romanize
   */
  public function testRomanizeRule3() {
    $this->assertEquals("bi-al-tamām̄", RomanizedTranslitFieldFormatter::romanize("bi al-tamām̄"));
    $this->assertEquals("wa-al-kamāl", RomanizedTranslitFieldFormatter::romanize("wa al-kamāl"));
    $this->assertEquals("bi-tarīq", RomanizedTranslitFieldFormatter::romanize("bi tarīq"));
  }
  
  /**
   * @covers ::romanize
   */
  public function testRomanizeRule4() {
    $this->assertEquals("lil-shirbini", RomanizedTranslitFieldFormatter::romanize("li al-shirbini"));
    $this->assertEquals("lil-Shirbīnī", RomanizedTranslitFieldFormatter::romanize("li’l-Shirbīnī"));
    $this->assertEquals("lil-Shirbīnī", RomanizedTranslitFieldFormatter::romanize("li-’l-Shirbīnī"));
    $this->assertEquals("lil-Shirbīnī", RomanizedTranslitFieldFormatter::romanize("liʾl-Shirbīnī"));
    $this->assertEquals("lil-Shirbīnī", RomanizedTranslitFieldFormatter::romanize("li-ʾl-Shirbīnī"));
    $this->assertEquals("li-tajrīd", RomanizedTranslitFieldFormatter::romanize("li tajrīd"));
  }
  
  /**
   * @covers ::romanize
   */
  public function testRomanizeRule5() {
    $this->assertEquals("fi al-kitāb", RomanizedTranslitFieldFormatter::romanize("fi’l-kitāb"));
    $this->assertEquals("fi al-kitāb", RomanizedTranslitFieldFormatter::romanize("fi-’l-kitāb"));
    $this->assertEquals("fi al-kitāb", RomanizedTranslitFieldFormatter::romanize("fiʾl-kitāb"));
    $this->assertEquals("fi al-kitāb", RomanizedTranslitFieldFormatter::romanize("fi-ʾl-kitāb"));
    $this->assertEquals("al-shams", RomanizedTranslitFieldFormatter::romanize("aš-šams"));
    $this->assertEquals("al-dhams", RomanizedTranslitFieldFormatter::romanize("aḏ-ḏams"));
  }

  /**
   * @covers ::romanize
   */
  public function testRomanizeSamples() {
    $this->assertEquals("Risālah fī al-ʿamal bi-rubʿ al-muqanṭarāt al-shamālīyah", 
        RomanizedTranslitFieldFormatter::romanize("Risālaẗ fī al-ʿamal bi-rubʿ al-muqanṭarāt al-šamālīyaẗ"));
    $this->assertEquals("Mukhtaṣarah fī ṣanʿah baʿḍ al-ālāt al-raṣadiyyah wa-al-ʿamal bi-hā", 
        RomanizedTranslitFieldFormatter::romanize("Muḫtaṣaraẗ fī ṣanʿaẗ baʿḍ al-ālāt al-raṣadiyyaẗ wa-ʾl-ʿamal bi-hā"));
    $this->assertEquals("al-Mulakhkhaṣ fī al-hayʾah al-basīṭah",
        RomanizedTranslitFieldFormatter::romanize("al-Mulaḫḫaṣ fī al-hayʾaẗ al-basīṭaẗ"));
  }
  
}
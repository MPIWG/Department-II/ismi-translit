<?php

namespace Drupal\search_api_transliteration;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Component\Transliteration\TransliterationInterface;
use Normalizer;

/**
 * Class SearchApiTransliteration.
 */
class SearchApiTransliteration implements TransliterationInterface {
  
  /**
   * {@inheritdoc}
   */
  public function transliterate($string, $langcode = 'en', $unknown_character = '?', $max_length = NULL) {
    $string = $this->normalizeArabicTranslit($string);    
    return $string;
  }
  
  /**
   * {@inheritdoc}
   */
  public function removeDiacritics($string) {
    // remove diacritics by de-composing and removing diacritical marks
    $string = Normalizer::normalize($string, Normalizer::FORM_D);
    $string = preg_replace('/\p{M}+/u', '', $string);
    return $string;
  }
  
  public function setConfig(ConfigEntityInterface $config) {
  }

  /**
   * Normalize text in Arabic transliteration.
   *
   * For details see
   * https://it-dev.mpiwg-berlin.mpg.de/tracs/OpenMind3/wiki/normalize_arabic_translit
   *
   * @param string $string
   * @return string
   *
   */
  protected function normalizeArabicTranslit($string) {
    // everything is lowercase TODO: locale?
    $string = mb_strtolower($string);

    // remove "apostrophes" (unicode escapes in PHP need double quotes!)
    $string = preg_replace("/'|\u{0060}|\u{02BE}|\u{02BF}|\u{2018}|\u{2019}/", '', $string);

    // replace special letters
    // ỳ -> a
    $string = preg_replace("/\u{1EF3}/", 'a', $string);
    // ı -> i
    $string = preg_replace("/\u{0131}/", 'i', $string);
    
    // remove diacritics by de-composing and removing diacritical marks
    $string = Normalizer::normalize($string, Normalizer::FORM_D);
    $string = preg_replace('/\p{M}+/u', '', $string);

    // replace two-letter combinations
    $string = preg_replace('/ch/', 'j', $string);
    $string = preg_replace('/dj/', 'j', $string);
    $string = preg_replace('/th/', 't', $string);
    $string = preg_replace('/kh/', 'h', $string);
    $string = preg_replace('/dh/', 'd', $string);
    $string = preg_replace('/sh/', 's', $string);
    $string = preg_replace('/gh/', 'g', $string);
    
    // replace word-parts
    // aẗ, at, ah at at word end -> a
    $string = preg_replace('/at\b|ah\b/', 'a', $string);
    // 'abd + space -> 'abd (now without apostrophe)
    $string = preg_replace('/abd /', 'abd', $string);
    
    // replace letters
    $string = preg_replace('/g|c/', 'j', $string);
    return $string;
  }
  
  
}

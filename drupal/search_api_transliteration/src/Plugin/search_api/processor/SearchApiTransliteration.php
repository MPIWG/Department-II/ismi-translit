<?php

namespace Drupal\search_api_transliteration\Plugin\search_api\processor;

use Drupal\search_api\Plugin\search_api\processor\Transliteration;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Makes searches insensitive to accents and other non-ASCII characters.
 *
 * @SearchApiProcessor(
 *   id = "search_api_transliteration",
 *   label = @Translation("Search api transliteration"),
 *   description = @Translation("Special transliteration."),
 *   stages = {
 *     "pre_index_save" = 0,
 *     "preprocess_index" = -20,
 *     "preprocess_query" = -20
 *   }
 * )
 */
class SearchApiTransliteration extends Transliteration {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // call parent create
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    // overwrite transliterator with our version
    $processor->setTransliterator(\Drupal::service('search_api_transliteration.transliteration'));
    return $processor;
  }
  
  /**
   * Retrieves the transliterator.
   *
   * @return \Drupal\Component\Transliteration\TransliterationInterface
   *   The transliterator.
   */
  public function getTransliterator() {
    // use our transliterator if it isn't set
    return $this->transliterator ?: \Drupal::service('search_api_transliteration.transliteration');
  }
}

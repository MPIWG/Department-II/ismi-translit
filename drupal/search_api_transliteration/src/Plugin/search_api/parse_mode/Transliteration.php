<?php

namespace Drupal\search_api_transliteration\Plugin\search_api\parse_mode;

use Drupal\search_api\ParseMode\ParseModePluginBase;

/**
 * Represents a parse mode that just passes the user input on as-is.
 *
 * @SearchApiParseMode(
 *   id = "search_api_transliteration",
 *   label = @Translation("Transliterate query"),
 *   description = @Translation("Use special transliteration."),
 * )
 */
class Transliteration extends ParseModePluginBase {

  /**
   * {@inheritdoc}
   */
  public function parseInput($keys) {
    return \Drupal::service('search_api_transliteration.transliteration')->transliterate($keys);
  }

}

<?php

namespace Drupal\Tests\search_api_transliteration\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\search_api_transliteration\SearchApiTransliteration;

/**
 * Provide functions for converting measurements.
 *
 * @coversDefaultClass \Drupal\search_api_transliteration\SearchApiTransliteration
 * @group search_api_transliteration
 */

class TransliterationUnitTest extends UnitTestCase {

  protected $service;

  protected function setUp() {
    $this->service = new SearchApiTransliteration();
  }

  public function testServiceInstance() {
    $this->assertInstanceOf(SearchApiTransliteration::class, $this->service);
  }

  public function testIsNotEmpty() {
    $this->assertNotEmpty($this->service->transliterate("test"));
  }

  public function testIsString() {
    $this->assertTrue(is_string($this->service->transliterate("test")));
  }


  /**
   * @covers ::transliterate
   */
  public function testGermanTransliteration() {
    $this->assertEquals('uummß', $this->service->transliterate("Üümmß"));
  }

  /**
   * @covers ::transliterate
   */
  public function testArabicTransliteration() {
    $this->assertEquals('test', $this->service->transliterate("test"));
    $this->assertEquals('haha', $this->service->transliterate('hỳhỳ'));
    $this->assertEquals('hahaha', $this->service->transliterate('ha\'ha’haʾ'));
    $this->assertEquals('blablat', $this->service->transliterate('blaẗblaẗ'));
    $this->assertEquals('jajmini', $this->service->transliterate('Jaghmini'));
    $this->assertEquals('saraf al-din mahmud ibn muhammad ibn umar al-jajmini al-hwarizmi', 
          $this->service->transliterate('Šaraf al-Dīn Maḥmūd ibn Muḥammad ibn ʿUmar al-Jaġmīnī al-Ḫwārizmī'));
    $this->assertEquals('jajmini', $this->service->transliterate('Jaghmını'));
  }
}
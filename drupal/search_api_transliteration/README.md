# Search api Transliteration

## Plugins

### Parse Mode: Transliteration

Add a parse mode for keywords, based on own transliteration service.

### Processor: Transliteration

Add a processor based on own transliteration service.

### Testing

Run in `<project>/docroot`.

```bash
 fin exec "php core/scripts/run-tests.sh --color --module search_api_transliteration --sqlite /tmp/test.sqlite --verbose"
 ```
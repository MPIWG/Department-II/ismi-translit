# ISMI system for normalizing Arabic transliterations #

Algorithm for normalizing the existing transliterated arabic (_translit fields) in the database.

## Rules ##

### 1. normalize "apostrophes" ###

Normalize apostrophe-like characters used for hamza and ain.

Replace:

| replace | with |
|--- | --- |
| `, ʿ, ‘, ’, ʾ | ' |

### 2. replace letter combinations ###

Replace the following letter combinations with a single letter:

| replace | with |
|--- | --- |
| dj, ch | j |
| th | t |
| kh | h |
| dh | d |
| sh | s |
| gh | g |

Replace at the end of a word:

| replace | with |
|--- | --- |
| aẗ\b, at\b, ah\b | a |

Replace:

| replace | with |
|--- | --- |
| "'abd " | 'abd |

Replace letters with diacritics:

| replace | with |
|--- | --- |
| ỳ | a |

### 3. remove diacritics ###

Replace all letters with diacritics with the letter without diacritics.

### 4. replace letters ###

Replace the following letters to unify the searches:

| replace | with |
|--- | --- |
| g, c | j |


### 5. remove apostrophes (optional) ###

Remove all apostrophes.


-------------
For reference:

http://docs.oracle.com/javase/7/docs/api/java/text/Normalizer.html

http://junidecode.sourceforge.net/

http://userguide.icu-project.org/transforms/general


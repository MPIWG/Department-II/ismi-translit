# ISMI Arabic transliteration to LOC transliteration

How to transform ISMI Arabic transliteration format into US Library of Congress (LOC) Arabic transliteration format http://www.loc.gov/catdir/cpso/romanization/arabic.pdf (by Chantal Wahbi, MPIWG).

| Rule No. | String (input) | Hexadecimal | String (output) | Hexadecimal | Rule | Example | Notes | Reference |
|---|---|---|---|---|---|---|---|---|
| 1a) | Ṯ / ṯ | 1E6E / 1E6F | th | 0074&0068 |  |  |  | See p.1 in LoC for Rules 1a)-1f) |
| 1b) | Ḫ / ḫ | 1E2A / 1E2B | kh | 006B&0068 |  |  |  |  |
| 1c) | Ḏ / ḏ | 1E0E / 1E0F | dh | 0064&0068 |  |  |  |  |
| 1d) | Š / š | 0160 / 0161 | sh | 0073&0068 |  |  |  |  |
| 1e) | Ġ / ġ | 0120 / 0121 | gh | 0067&0068 |  |  |  |  |
| 1f) | ỳ | 1EF3 | á | 00E1 |
| 2a) | [Not beginnig with: al-] Xẗ al-X | X + 1E97 + 20&61&6C&2D + X | Xt al-X | X + 0074 + 20&61&6C&2D + X | When the word ending in ẗ is in the construct state [muḍāf wa-muḍāf ilayh], ẗ is normalized t. | Mir’āt al-zamān | Rule 2a) has priority over rule 2b). | In LoC: rule no. 7 (b)  |
| 2b) | Xẗ [Not followed by: al-X] | X + 1E97&20 | Xh | X +0068&20 | When the noun or adjective (X) ending in ẗ is indefinite, ẗ is normalized h. | ṣalāh |  | In LoC: rule no. 7 (a) |
| 2c) | Al-Xẗ | 20&61&6C&2D + X + 1E97 | Al-Xh | 20&61&6C&2D + X +0068 | When the noun or adjective (X) is ending in ẗ is preceded by the definite article (Al-), ẗ is normalized h.  | Al-Risālah al-bahiyyah | Rule 2c) has priority over rule 2a). | In LoC: rule no. 7 (a) |
| 2d) | Xẗan | X+1E97+ 61&6E | Xtan | X+0074+ 61&6E | When the word ending in ẗan, ẗ is normalized t (tan). | Faj’atan | | In LoC: rule  no. 7 (c) |
| 3a) | P al-X; P=[ bi; wa; ka] | [20&62&69/ 20&77&61/ 20&6B&61] + 20&61&6C&2D + X | P-al-X | [20&62&69/ 20&77&61/ 20&6B&61] + 2D&61&6C&2D + X | Inseparable prepositions, conjunctions, and other prefixes are connected with the article of the following word by a hyphen. | bi-al-tamām wa-al-kamāl |  | In Loc: Rule no. 17 (a) |
| 3b) | P X; P=[ bi; wa; ka] | [20&62&69/ 20&77&61/ 20&6B&61] +SPACE+X | P-X | [20&62&69/ 20&77&61/ 20&6B&61] + 2D+ X | Inseparable prepositions, conjunctions, and other prefixes are connected with what follows by a hyphen. | bi-tarīq |  | Rule no. 16 (b) |
| 4a) | li  | 20&6C&69 | li- | 20&6C&69&2D | See rule above | li-tajrīd |  | In LoC: rule no. 16 (b) |
| 4b) | li al-X / li’l-X / li-’l-X/ li-l-X | 20&6C&69+20&61&6C&2D + X/ 20&6C&69&27&6C&2D + X/ 20&6C&2D&27&6C&2D + X/ 20&6C&2D&6C&2D + X | lil-X | 20&6C&69&6C&2D + X | exceptional treatment of the preposition li followed by the article (al-) | lil-Shirbīnī | Rule 4b) has priority over rule 4a)  and 5a). | In LoC: rule no. 17 (b) |
| 5a) | ’l-X /   X-’l-X | 20&27&6C&2D + X/ X + 2D&27&6C&2D + X |  al-X | 20&61&6C&2D +X | When al is initial in the word, and when it follows an inseparable preposition or conjunction, it is always normalized al regardless of whether the preceding word, as romanized, ends in a vowel or a consonant. | fī al-kitāb | Rule 5a) has priority over 3a) | In LoC: rule no. 17 (b) |
| 5b) | aY-YX; Y=Sun letters[t;  ṯ; d;  ḏ; r; z; s;  š;  ṣ;  ḍ;  ṭ;  ẓ; l; n]        | 20&61&[74/ 1E6F/ 64/ 1E0F/ 72/ 7A/ 73/ 0161/ 1E63/ 1E0D/ 1E6D/ 1E93/ 6C/ 6E]&2D&[74/ 1E6F/ 64/ 1E0F/ 72/ 7A/ 73/ 0161/ 1E63/ 1E0D/ 1E6D/ 1E93/ 6C/ 6E] + X | al-YX | 20&61&6C&2D&[74/ 1E6F/ 64/ 1E0F/ 72/ 7A/ 73/ 0161/ 1E63/ 1E0D/ 1E6D/ 1E93/ 6C/ 6E] + X | The l of the article is always normalized l, whether it is followed by a „sun letter“ or not/ regardless of whether or not it is assimilated in pronounciation to the initial consonant of the word to which it is attached. | al-šams (instead of: aš-šams) |  | In LoC: rule no. 17 (c) |
| 6) | λh; λ= [t; k; d; s; g] | [74/ 6B/ 64/ 73/ 67]&68 | λ’h  | [74/ 6B/ 64/ 73/ 67]&27&68 | (ʹ ) is used to separate two letters representing two distinct consonantal sounds, when the combination might otherwise be read as a digraph.  | Ad’ham |  | In LoC: rule no. 21 (a) |
| 7a) | X[illāh; ullāh; allah; allāh; -Allāh;   Allah; ullah] | X+ [69&6C&6C&101&68/ 75&6C&6C&101&68/ 61&6C&6C&61&68/ 61&6C&6C&101&68/ 2D&41&6C&6C&101&68/ 20&41&6C&6C&61&68/ 75&6C&6C&61&68] | X Allāh | X + 20&41&6C&6C&101&68 |  | ’Abd Allāh |  | In LoC: rule no. 23 |
| 7 b) | [ l; b; bism]illāh | [20&6C/ 20&62/ 20&62&69&73&6D/] + 69&6C&6C&101&68 | lillāh/ billāh/ bismillāh | 20&62&6C&6C&101&68/ 20&6C&6C&6C&101&68/ 20&62&69&73&6D&69&6C&6C&101&68 | Combinations of preposition + Allāh remain the same | lillāh billāh bismillāh | Rule no. 7b) has priority over 7a) | In LoC: rule no. 23 |


package de.mpg.mpiwg.ismi.analyzer;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardTokenizer;

/**
 * Analyzer for text using ISMI arabic transliteration.
 * 
 * @author casties
 *
 */
public class IsmiTranslitAnalyzer extends Analyzer {

	@Override
	protected TokenStreamComponents createComponents(String fieldName) {
        StandardTokenizer src = new StandardTokenizer();
        TokenStream result = new IsmiTranslitTokenFilter(src);
        return new TokenStreamComponents(src, result);
    }

}

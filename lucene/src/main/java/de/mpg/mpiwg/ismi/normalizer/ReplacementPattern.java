/**
 * 
 */
package de.mpg.mpiwg.ismi.normalizer;

import java.util.regex.Pattern;

/**
 * @author casties
 *
 */
public class ReplacementPattern {
	public Pattern pattern;
	public String replacement;
	
	/**
	 * @param replacement
	 * @param pattern
	 */
	public ReplacementPattern(String replacement, Pattern pattern) {
		super();
		this.pattern = pattern;
		this.replacement = replacement;
	}

	/**
	 * @return the pattern
	 */
	public Pattern getPattern() {
		return pattern;
	}

	/**
	 * @param pattern the pattern to set
	 */
	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	/**
	 * @return the replacement
	 */
	public String getReplacement() {
		return replacement;
	}

	/**
	 * @param replacement the replacement to set
	 */
	public void setReplacement(String replacement) {
		this.replacement = replacement;
	}
}

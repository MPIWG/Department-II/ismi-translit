package de.mpg.mpiwg.ismi.analyzer;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import de.mpg.mpiwg.ismi.normalizer.ArabicTranslitNormalizer;

/**
 * TokenFilter that normalizes text using ISMI arabic transliteration rules.
 * 
 * @author casties
 */
public class IsmiTranslitTokenFilter extends TokenFilter {

	protected IsmiTranslitTokenFilter(TokenStream input) {
		super(input);
	}

	/** Token char term attribute */
	private final CharTermAttribute charTerm = addAttribute(CharTermAttribute.class);

	/*
	 * Process the next token in the stream.
	 */
	@Override
	public final boolean incrementToken() throws java.io.IOException {
		if (!input.incrementToken()) {
			return false;
		}
		// get term as String
		final String text = charTerm.toString();
		// normalize String
		final String normText = ArabicTranslitNormalizer.normalize(text);
		// copy back to term if necessary
        if (!text.equals(normText)) {
        	// something changed
        	int newlen = normText.length();
        	charTerm.copyBuffer(normText.toCharArray(), 0, newlen);
        }
		return true;
	}
}

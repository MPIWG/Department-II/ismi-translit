package de.mpg.mpiwg.ismi.analyzer;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.junit.Test;

import de.mpg.mpiwg.ismi.analyzer.IsmiTranslitAnalyzer;

/**
 * @author casties
 *
 */
public class IsmiTranslitAnalyzerTest {

	public List<String> analyze(String text, Analyzer analyzer) throws IOException {
	    List<String> result = new ArrayList<String>();
	    TokenStream tokenStream = analyzer.tokenStream("tokens", text);
	    CharTermAttribute attr = tokenStream.addAttribute(CharTermAttribute.class);
	    tokenStream.reset();
	    while(tokenStream.incrementToken()) {
	       result.add(attr.toString());
	    }       
	    return result;
	}

	@Test
	public void translitAnalyzer_analyze1() throws IOException {
	    List<String> result = analyze("Ṯ/ṯ Ḫ/ḫ Ḏ/ḏ Š/š Ġ/ġ ỳ ı", new IsmiTranslitAnalyzer());	 
	    assertThat(result, 
	      hasItems("t", "t", "h", "h", "d", "d", "s", "s", "j", "j", "a", "i"));
	}

	@Test
	public void translitAnalyzer_analyze2() throws IOException {
	    List<String> result = analyze("Th/th Kh/kh Dh/dh Sh/sh Gh/gh a", new IsmiTranslitAnalyzer());	 
	    assertThat(result, 
	      hasItems("t", "t", "h", "h", "d", "d", "s", "s", "j", "j", "a"));
	}

	@Test
	public void translitAnalyzer_analyze3() throws IOException {
	    List<String> result = analyze("fatḥ FATḤ", new IsmiTranslitAnalyzer());	 
	    assertThat(result, 
	      hasItems("fath", "fath"));
	}

	@Test
	public void translitAnalyzer_analyze4() throws IOException {
	    List<String> result = analyze("Muḫtaṣaraẗ fī ṣanʿaẗ baʿḍ al-ālāt al-raṣadiyyaẗ wa-ʾl-ʿamal bi-hā", 
	    	new IsmiTranslitAnalyzer());	 
	    assertThat(result, 
	    	hasItems("muhtasara", "fi", "sana", "bad", "al", "alat", "al", "rasadiyya", "wa", "l", "amal", "bi", "ha"));
	}

}
